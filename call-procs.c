#include <math.h>  // Libreria matematica
#include <stdio.h> // Standard Input/Output library
#include <mpi.h>   // Libreria para MPI

int main(int argc, char** argv)
{
    // Variables para MPI
    int num_processes;
    int curr_rank;
    int proc_name_len;
    char proc_name[MPI_MAX_PROCESSOR_NAME];
    
    // Inicializamos MPI
    MPI_Init (&argc, &argv);
    
    // numero de procesos
    MPI_Comm_size(MPI_COMM_WORLD, &num_processes);
    
    // rank del proceso actual
    MPI_Comm_rank(MPI_COMM_WORLD, &curr_rank);
    
    // Nombre del proceso para el hilo actual
    MPI_Get_processor_name(proc_name, &proc_name_len);
    
    // rank, No. de proceso y nombre del proceso
    printf("Llamada al proceso %d de %d en %s \n", curr_rank, num_processes, proc_name);
    
    // Finalizamos la llamada a MPI
    MPI_Finalize();
    
    return 0;
}
